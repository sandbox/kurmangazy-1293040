<?php

function nice_phone_numbers_admin_form($form) {
  $patterns = variable_get('nice_phone_numbers_patterns');
  $form['patterns'] = array(
    '#type' => 'textarea',
    '#cols' => 40,
    '#rows' => 10,
    '#default_value' => implode("\n", $patterns),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

// The pattern algorithm is currently able to be tripped up by characters
// with special meanings to the PHP and javascript regex functions, like $.
// A complete solution that permitted those characters, and provided escaping
// for characters that are special in this system, like . and [, would be
// tricky to write (though certainly possible).

// For now, it would be helpful if somebody went through and found all the
// problematic characters, and tested for them here, so it could throw a
// validation error.

// We also should ensure that all bracket expressions are well-formed.

function nice_phone_numbers_admin_form_validate($form, &$form_state) {
  if(!trim($form_state['values']['patterns'])) {
    form_set_error('patterns', t('Please enter at least one pattern.'));
  }
}

function nice_phone_numbers_admin_form_submit($form, &$form_state) {
  $input_string = $form_state['values']['patterns'];
  $lines = explode("\n", $input_string);

  $patterns = array();
  foreach($lines as $line) {
    // make sure to avoid problems caused by invisible whitespace and newlines
    $pat = trim($line);
    if($pat) {
      $patterns[] = $pat;
    }
  }

  variable_set('nice_phone_numbers_patterns', $patterns);
  drupal_set_message(t('Settings have been saved.'));
}
