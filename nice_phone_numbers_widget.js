(function($) {

/**
 * Get the position of the cursor in a textfield
 *
 * I think this duplicates Drupal.getSelection(). If so, use that instead.
 */
$.fn.nicePhoneNumbersGetTextCursorPos = function () {
  var ctrl = this[0];
  var CaretPos = [];
  // IE Support
  if (document.selection) {

      ctrl.focus ();
      var Sel = document.selection.createRange ();

      //$('#edit-field-art-fax-und-0-extension').val(Sel.text);

      CaretPos[0] = Sel.text.length;
      Sel.moveStart ('character', -ctrl.value.length);

      CaretPos[1] = Sel.text.length;
      CaretPos[0] = CaretPos[1] - CaretPos[0];

  }
  // Firefox support
  else if (ctrl.selectionStart || ctrl.selectionStart == '0') {
      CaretPos[0] = ctrl.selectionStart;
      CaretPos[1] = ctrl.selectionEnd;
  }

  return (CaretPos);
}

/**
 * Set the position of the cursor in a textfield
 *
 * I found this on the web somewhere and made minor mods
 */
$.fn.nicePhoneNumbersSetTextCursorPos = function (pos) {
  return this.each(function() {
    var ctrl = this;
    if(ctrl.setSelectionRange) {
      ctrl.focus();
      ctrl.setSelectionRange(pos,pos);
    }
    else if (ctrl.createTextRange) {
      var range = ctrl.createTextRange();
      range.collapse(true);
      range.moveEnd('character', pos);
      range.moveStart('character', pos);
      range.select();
    }
  });
}




/**
 * Get data about the current state of the phone number entry textfield
 *
 * @return
 *   An object with the following keys:
 *   - number: The content of the textfield with non-digits stripped out.
 *   - cursor: The absolute offset of the cursor within the textfield.
 *   - numberPos: The cursor position as an index into 'number.'
 */
var getPhoneNumberTextFieldInfo = function(textField) {
  var text = $(textField).val();
  var number = text.replace(/[^0-9]/g, '');
  var cursor = $(textField).nicePhoneNumbersGetTextCursorPos();
  var dots, numberPos = [];
  var exprNum = 0;

  if(number === '') {
    return { number: number, cursor: cursor, numberPos: [0,0] };
  }

  numberPos[0] = numberPos[1] = text.slice(0, cursor[0]).replace(/[^0-9]/g, '').length;
  if(cursor[0] !== cursor[1]) {
    numberPos[1] += text.slice(cursor[0], cursor[1]).replace(/[^0-9]/g, '').length;
  }

  return { number: number, cursor: cursor, numberPos: numberPos };
}


/**
 * Set the new state for the phone number entry textfield
 *
 * @param textField
 * @param info
 *   An object with the following keys:
 *   - number: The new content of the textfield as a string of digits.
 *   - cursor: The offset within the textfield to move the cursor to.
 *   - numberPos: The index into 'number' that describes where the cursor should go.
 *   'cursor' and 'numberPos' should never both be set. At least one should be null.
 *   If 'cursor' and 'numberPos' are both null, the cursor will be left alone.
 */
var rebuildPhoneNumberTextField = function(textField, info) {
  var exprNum = nicePhoneNumbers.getMatchingPattern(info.number);
  var text = info.number;

  if(exprNum === null) {
    return;
  }

  if(text !== '') {
    var expr = new RegExp(nicePhoneNumbers.data.expressions[exprNum][text.length-1], '');
    text = text.replace(expr, nicePhoneNumbers.data.replacements[exprNum][text.length-1]);
  }

  $(textField).val(text);

  // set cursor back
  if(info.cursor !== null) {
    $(textField).nicePhoneNumbersSetTextCursorPos(info.cursor);
  }
  else if(info.numberPos !== null) {
    // get the length of the portion of the number before the cursor
    var newCursorPos = info.numberPos === 0 ? 0 : nicePhoneNumbers.data.fragmentLengths[exprNum][info.numberPos-1];
    $(textField).nicePhoneNumbersSetTextCursorPos(newCursorPos);
  }
};


Drupal.behaviors.nicePhoneNumbers = {
  attach: function(context, settings) {
    if(!nicePhoneNumbers.data) {
      nicePhoneNumbers.initData(settings.nicePhoneNumbers.patterns);
    }

    $('.nice-phone-number-input', context).keydown(function(event) {

      var textInfo = getPhoneNumberTextFieldInfo(this);

      var key = event.which;
      if(key === 46) { // delete key pressed
        if(textInfo.numberPos[0] === textInfo.numberPos[1]) {
          if(textInfo.numberPos[0] < textInfo.number.length) {
            // remove the digit to the right of the cursor
            textInfo.number = textInfo.number.slice(0, textInfo.numberPos[0]) + textInfo.number.slice(textInfo.numberPos[0]+1);
          }
          else {
            return false;
          }
        }
        else { // remove all the digits inside the selection
          textInfo.number = textInfo.number.slice(0, textInfo.numberPos[0]) + textInfo.number.slice(textInfo.numberPos[1]);
        }

        textInfo.numberPos = null; // we want to set the cursor position back to an absolute position
        textInfo.cursor = textInfo.cursor[0]; // the cursor ends up where the selection started
        rebuildPhoneNumberTextField(this, textInfo);

      }
      // these are arrow keys, end, home, shift, ctrl, tab, and esc, respectively. let the browser handle them
      if(((key >= 37) && (key <= 40)) || (key === 35) || (key === 36) || (key === 16) || (key === 17) || (key === 9) || (key === 27)) {
        return true;
      }
      if(key === 45) { // this is insert. IE toggles insert mode, while FF does nothing.
      }
      if(key === 8) { // backspace key pressed
        if(textInfo.numberPos[0] === textInfo.numberPos[1]) {
          if(textInfo.numberPos[0] > 0) {
            // remove the digit to the left of the cursor & decrease the number position by one
            textInfo.number = textInfo.number.slice(0, textInfo.numberPos[0]-1) + textInfo.number.slice(textInfo.numberPos[1]);
            textInfo.numberPos[0]--;
          }
          else {
            return false;
          }
        }
        else {
          // remove all the digits inside the selection, same as delete, & the number position stays the same
          textInfo.number = textInfo.number.slice(0, textInfo.numberPos[0]) + textInfo.number.slice(textInfo.numberPos[1]);
        }

        textInfo.cursor = null; // recompute the absolute cursor position
        textInfo.numberPos = textInfo.numberPos[0];
        rebuildPhoneNumberTextField(this, textInfo);
      }
      // adjust the key number in case it's on the keypad
      var keyDigit = (key - 48 < 48) ? key : key - 48;

      if(keyDigit >= 48 && keyDigit <= 57) { // number key pressed
        // add a digit to the right of the cursor, and move the cursor to the right
        // I can add the digit to my copy of the number even if there are too many digits/invalid digit, because rebuildPhoneNumberTextField checks and gracefully rejects it
        textInfo.number = textInfo.number.slice(0, textInfo.numberPos[0]) + String.fromCharCode(keyDigit) + textInfo.number.slice(textInfo.numberPos[1]);
        textInfo.numberPos = textInfo.numberPos[0] + 1;
        textInfo.cursor = null;
        rebuildPhoneNumberTextField(this, textInfo);
      }
      return false;
    });

    $('.nice-phone-number-input', context).each(function() {
      // update the number input field
      var text = $(this).val();
      var number = text.replace(/[^0-9]/g, '');

      // set numberPos = cursor = null to disable setting cursor position (issue #1349894)
      var textInfo = { number: number, numberPos: null, cursor: null };

      rebuildPhoneNumberTextField(this, textInfo);
    });
  }
};

})(jQuery);
