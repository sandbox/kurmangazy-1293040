var nicePhoneNumbers = {};

(function($) {

nicePhoneNumbers.initData = function(patterns) {
  this.data = {};
  var that = this;

  // patterns is the list of input patterns as complete strings
  // patterns are matched from left to right, so more restrictive patterns have to come before more permissive patterns

  // expressions is the fragments with dots replaced by '\d' for String.replace
  this.data.expressions = [];

  // replacements is the fragments with dots replaced by '$n' for String.replace
  this.data.replacements = [];

  // fragmentLengths is the length of each fragment prefix
  this.data.fragmentLengths = [];

  $.each(patterns, function(i, pattern) {
    // break the replacement pattern into one piece per digit
    var pieces = pattern.match(/[^\[?0-9]*(?:[?0-9]|\[[^\]]*\])[^\[?0-9]*/g);
    var str = '';
    var fragmentLengths = [];
    var expressions = [];
    var replacements = [];

    $.each(pieces, function(j, piece) {
      str += piece;

      // convert the character classes into single characters and compute the length
      fragmentLengths.push(str.replace(/\[[^\]]*\]/g, '?').length);

      // remove everything that doesn't match a digit to generate a regular expression
      expressions.push('^' + str.replace(/[^\[?0-9]*([?0-9]|\[[^\]]*\])[^\[?0-9]*/g, '($1)').replace(/\(\?\)/g, '(\\d)'));

      // convert the question marks and digits in the fragments into $n for String.replace()
      (function() {
        var cnt=1;
        replacements.push(str.replace(/[?0-9]|\[[^\]]*\]/g, function(match) {
          return '$' + cnt++;
        }));
      })();
    });

    that.data.fragmentLengths.push(fragmentLengths);
    that.data.expressions.push(expressions);
    that.data.replacements.push(replacements);
  });
};


nicePhoneNumbers.getMatchingPattern = function(number) {
  var exprNum = 0;
  if(number === '') {
    return 0;
  }
  for(; exprNum<this.data.expressions.length; exprNum++) {
    if(number.length <= this.data.expressions[exprNum].length) {
      var regex = new RegExp(this.data.expressions[exprNum][number.length-1], '');
      if(regex.test(number)) {
        break;
      }
    }
  }
  //alert('matched: ' + nicePhoneNumbers.expressions[exprNum][number.length-1]);
  return exprNum === this.data.expressions.length ? null : exprNum;
};


})(jQuery);
